## FIRMWARE DUMP
### sys_tssi_64_armv82_infinix-user 12 SP1A.210812.016 700479 release-keys
- Transsion Name: Infinix NOTE 12 2023
- TranOS Build: X676C-H891ZAaAbAc-S-OP-240912V1816
- TranOS Version: xos10.6.0
- Brand: INFINIX
- Model: Infinix-X676C
- Platform: mt6789 (Helio G99)
- Android Build: SP1A.210812.016
- Android Version: 12
- Kernel Version: 5.10.198
- Security Patch: 2024-09-05
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Screen Density: 480
